$(function() {
    for (var color = 0; color < 4; color++) {
        for(var hue=1; hue <= 5; hue++) {
            $('<li></li>', {
                    text: 'color ' + color + hue,
            }).appendTo('ul.menu');
        }
    }
});
